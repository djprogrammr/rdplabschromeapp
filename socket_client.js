var port = null;
var socket = null;
publicIp = "";
var getKeys = function(obj){
   var keys = [];
   for(var key in obj){
      keys.push(key);
   }
   return keys;
}



function onNativeMessage(message) {
  if(message.hasOwnProperty("ip")){
    console.log("found ip="+message.ip);
    publicIp = message.ip;
	_getMachineMode(publicIp);
    connect();
  }  
}

function onDisconnected() {
  showNotification('Krazykoder', "Failed to connect ");
  port = null;
}

function connect() {
	
	
  //socket = io.connect('http://ec2-34-226-213-112.compute-1.amazonaws.com:3002');
  socket = io.connect('https://www.krazykoder.com:3002');
  socket.on('connect', function() {
	  console.log("connected");
     showNotification('Krazykoder', 'Connected to Krazykoder server!');
     socket.emit('connectRoom', { ip: publicIp });
     updateStatusBar('online');
  });
  
  socket.on('setTestData', function(data) {
	  console.log('setTestData event recieved');
	  console.log(data);
	 testData = data;
	 solution = data;
	 _triggerPluginDisplay(data);
     socket.emit('onCandidateMachineSetupComplete', data);
  });

  socket.on('setMachineEnv', function(data) {
     socket.emit('connectRoom', { ip: publicIp });
     launchMode = data.mode;
     //_triggerPluginDisplay(data);
  });

  socket.on('disconnect', function(){
    updateStatusBar('offline');
    onDisconnected();
  });

  socket.on('git-checkout', function(data) {
    console.log('GIT checout branch='+data.branch);
    showNotification('Krazykoder', 'You are currently viewing '+ data.candidateName +'\'s solution.');
    _updateRecruiterView(data);
    message = {"action":"gitcheckout","checkout": data.branch};

    try {
      port.postMessage(message);
    } catch (err) {
      console.log('Failed to trigger git command.')
    }
    
    data.status = 'success';
    socket.emit('sendGitCheckoutStatus', data);
  });
  
  socket.on('triggerFinishTest', function(){
    //dj: commented for now as event was getting fired infinitely in a loop
	//_finishTest();
  });
  
 
}

function triggerFinishTest() {
	console.log('triggering finish test..');
	if(socket)
		socket.emit('onFinishTest', { ip: publicIp });
}

function connectHost(){
  var hostName = "com.google.chrome.example.echo";
  port = chrome.runtime.connectNative(hostName);
  port.onMessage.addListener(onNativeMessage);
  port.onDisconnect.addListener(onDisconnected);
  //updateUiState();
}

function getMyIp(){
  
  message = {"action":"getip","checkout":"na"};

  try {
    port.postMessage(message);
  } catch (err) {
    console.log('Failed to get Ip address.')
  }
}

function sendFinishTestEvent(){
	console.log("in send finish test");
	//triggers the electron app which will send Finish test to Electron Screen Recorder vis messenger.js
	message = {"action":"finishtest","checkout": "na"};
    port.postMessage(message);
	console.log("finish test sent");
}

document.addEventListener('DOMContentLoaded', function () {
    
  connectHost();
  getMyIp();
  //updateUiState();
});